import QtQuick 2.0
import Sailfish.Silica 1.0

CoverBackground {

    property int currentBeat: metronome.currentBeat

    Column {
        width: parent.width -60
        anchors{
            top: parent.top
            horizontalCenter: parent.horizontalCenter
            topMargin: 30
        }
        spacing: 30

        Image{
            source: "image://theme/icon-s-duration"
        }

        Label {
            id: label
            anchors.horizontalCenter: parent.horizontalCenter
            text: "%1 bpm".arg(metronome._bpm)
        }
    }

    CoverActionList {
        id: coverAction

        CoverAction {
            iconSource: metronome._running ? "image://theme/icon-cover-pause" : "image://theme/icon-cover-play"
            onTriggered: metronome._running = !metronome._running
        }

    }
}


