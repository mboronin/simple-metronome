import QtQuick 2.0
import Sailfish.Silica 1.0
import QtMultimedia 5.0

//import org.nemomobile.keepalive 1.1

import "../components"

Page {
    id: metronome

    width: parent.width
    height: parent.height

    property int currentBeat: 0
    property alias _beats: beats.value
    property int _bpm: tempo.value
    property alias _running: metronomeTimer.running

    onStatusChanged: {
        if (status === PageStatus.Activating) {
            tempo.maximumValue = storage.getValue("tempo/to") == null ? 300 : storage.getValue("tempo/to")
            tempo.minimumValue = storage.getValue("tempo/from") == null ? 30 : storage.getValue("tempo/from")
            tempo.stepSize = storage.getValue("tempo/step") == null ? 10 : storage.getValue("tempo/step")

            tempo.value = tempo.minimumValue
        }
    }

    Audio{ id: audio }

    SoundEffect{
        id: bip
        source: "qrc:/bip.wav"
        volume: audio.volume
    }

    SoundEffect {
        id: bop
        source: "qrc:/bop.wav"
        volume: audio.volume
    }

    Timer {
        id: metronomeTimer
        interval: 60000 / tempo.value
        repeat: true

        onRunningChanged: {
            if (running) {
                bip.play();
            }
            else {
                currentBeat = 0;
            }
        }

        onTriggered: {
            currentBeat = (currentBeat + 1)%beats.value
            if(currentBeat === 0) {
                bip.play()
                return
            }
            bop.play()
        }
    }

    SilicaFlickable {
        anchors.fill: parent

        PullDownMenu {
            MenuItem {
                text: qsTr("Settings")
                onClicked: {
                    metronome._running = false


                    pageStack.push(Qt.resolvedUrl("SettingsPage.qml"))
                }
            }
        }

        PageHeader {
            id: title
            title: "Metronome"
        }

        Column {
            width: parent.width - (Theme.paddingLarge * 2)
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: title.bottom

            spacing: Theme.paddingLarge



            // Selector for number of beats
            MeterSelector {
                id: beats
                anchors.horizontalCenter: parent.horizontalCenter
            }


            // Show Current beat 
            Label{
                text: (currentBeat + 1)
                font.pixelSize: 140
                anchors.horizontalCenter: parent.horizontalCenter
            }

            Slider{
                id: tempo

                width: parent.width
                valueText: qsTr("%1 BPM").arg(value)
                label: qsTr("Tempo")
            }

            // Start-stop button
            MouseArea{
                width: parent.width
                height: width
                Image {
                    anchors.centerIn: parent
                    source: metronomeTimer.running ? "image://theme/icon-l-pause" : "image://theme/icon-l-play"
            }
                onClicked: {
                    metronomeTimer.running = !metronomeTimer.running;

            }
        }

        }


}


}
