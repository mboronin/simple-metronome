#ifdef QT_QML_DEBUG
#include <QtQuick>
#endif

#include <sailfishapp.h>

#include <QtGlobal>
#include <QQmlEngine>
#include <QQmlExtensionPlugin>

int main(int argc, char *argv[])
{

    return SailfishApp::main(argc, argv);
}

